package com.dolhalova_testtask.domain.model

import java.util.Date

data class BootEvent(
    val id: Long,
    val occurredAt: Date
)