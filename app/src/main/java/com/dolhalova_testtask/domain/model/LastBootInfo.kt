package com.dolhalova_testtask.domain.model

import java.util.Date

sealed class LastBootInfo {
    data object Absent : LastBootInfo()
    data class One(val occurredAt: Date) : LastBootInfo()
    data class Many(val lastDelta: Long) : LastBootInfo()
}