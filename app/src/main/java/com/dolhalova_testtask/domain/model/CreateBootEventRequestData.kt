package com.dolhalova_testtask.domain.model

import java.util.Date

data class CreateBootEventRequestData(
    val occurredAt: Date
)