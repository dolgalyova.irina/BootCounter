package com.dolhalova_testtask.domain.model

data class NotificationDismissalSettings(
    val dismissalInterval: Int,
    val dismissalCount: Int
)