package com.dolhalova_testtask.domain

import com.dolhalova_testtask.data.bootEvent.BootEventRepository
import com.dolhalova_testtask.data.dismissalSettings.DismissalSettingsRepository
import com.dolhalova_testtask.domain.model.BootEvent
import com.dolhalova_testtask.domain.model.CreateBootEventRequestData
import com.dolhalova_testtask.domain.model.LastBootInfo
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings
import kotlinx.coroutines.flow.Flow
import java.util.Date

private const val LAST_EVENT_COUNT = 2

class BootEventService(
    private val eventRepository: BootEventRepository,
    private val dismissalSettingsRepository: DismissalSettingsRepository
) {

    fun getEvents(): Flow<List<BootEvent>> = eventRepository.getEvents()

    suspend fun createEvent() {
        val request = CreateBootEventRequestData(
            occurredAt = Date()
        )
        eventRepository.createBootEvent(request)
    }

    suspend fun getLastBootInfo(): LastBootInfo {
        val lastEvents = eventRepository.getLastEvents(limit = LAST_EVENT_COUNT)
        return when (lastEvents.size) {
            0 -> LastBootInfo.Absent
            1 -> LastBootInfo.One(lastEvents.first().occurredAt)
            else -> LastBootInfo.Many(
                lastDelta = lastEvents.first().occurredAt.time - lastEvents.last().occurredAt.time
            )
        }
    }

    suspend fun saveDismissalSettings(data: NotificationDismissalSettings) {
        dismissalSettingsRepository.storeDismissalSettings(data)
    }

    suspend fun getDismissalSettings(): NotificationDismissalSettings {
        return dismissalSettingsRepository.getDismissalSettings()
    }
}