package com.dolhalova_testtask.data.dismissalSettings

import com.dolhalova_testtask.data.dismissalSettings.local.DismissalSettingsLocalSource
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings

class DismissalSettingsRepository(
    private val localSource: DismissalSettingsLocalSource
) {

    suspend fun storeDismissalSettings(data: NotificationDismissalSettings) {
        localSource.storeDismissalSettings(data)
    }

    suspend fun getDismissalSettings(): NotificationDismissalSettings {
        return localSource.getDismissalSettings()
    }
}
