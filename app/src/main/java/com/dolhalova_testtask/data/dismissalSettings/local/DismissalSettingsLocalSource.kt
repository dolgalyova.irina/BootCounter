package com.dolhalova_testtask.data.dismissalSettings.local

import android.content.SharedPreferences
import androidx.core.content.edit
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings

private const val KEY_DISMISSAL_COUNT = "dismissal_count"
private const val KEY_DISMISSAL_INTERVAL = "dismissal_interval"

class DismissalSettingsLocalSource(
    private val prefs: SharedPreferences
) {

    fun storeDismissalSettings(data: NotificationDismissalSettings) = prefs.edit {
        putInt(KEY_DISMISSAL_INTERVAL, data.dismissalInterval)
        putInt(KEY_DISMISSAL_COUNT, data.dismissalCount)
    }

    fun getDismissalSettings(): NotificationDismissalSettings {
        return NotificationDismissalSettings(
            dismissalInterval = prefs.getInt(KEY_DISMISSAL_INTERVAL, 20),
            dismissalCount = prefs.getInt(KEY_DISMISSAL_COUNT, 5)
        )
    }
}
