package com.dolhalova_testtask.data.bootEvent.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dolhalova_testtask.data.bootEvent.local.entity.BootEventEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BootEventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun storeEvent(data: BootEventEntity): Long

    @Query("SELECT * FROM boot_events ORDER BY timestamp DESC LIMIT :limit")
    suspend fun getLastEvents(limit: Int): List<BootEventEntity>

    @Query("SELECT * FROM boot_events ORDER BY timestamp DESC")
    fun getEvents(): Flow<List<BootEventEntity>>
}