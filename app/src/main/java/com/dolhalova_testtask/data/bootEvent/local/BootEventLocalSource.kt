package com.dolhalova_testtask.data.bootEvent.local

import com.dolhalova_testtask.data.bootEvent.local.dao.BootEventDao
import com.dolhalova_testtask.data.bootEvent.local.entity.BootEventEntity
import com.dolhalova_testtask.data.bootEvent.local.mapper.toModel
import com.dolhalova_testtask.domain.model.CreateBootEventRequestData
import com.dolhalova_testtask.domain.model.BootEvent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class BootEventLocalSource(private val dao: BootEventDao) {

    suspend fun createEvent(data: CreateBootEventRequestData) {
        val event = BootEventEntity.create(data.occurredAt.time)
        dao.storeEvent(event)
    }

    suspend fun getLastEvents(limit: Int): List<BootEvent> {
        return dao.getLastEvents(limit = limit).map(BootEventEntity::toModel)
    }

    fun getEvents(): Flow<List<BootEvent>> {
        return dao.getEvents().map { events ->
            events.map(BootEventEntity::toModel)
        }
    }
}