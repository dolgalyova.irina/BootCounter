package com.dolhalova_testtask.data.bootEvent.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "boot_events")
data class BootEventEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "timestamp") val timestamp: Long
) {
    companion object {
        private const val NO_ID = 0L

        fun create(timestamp: Long) = BootEventEntity(
            id = NO_ID,
            timestamp = timestamp
        )
    }
}