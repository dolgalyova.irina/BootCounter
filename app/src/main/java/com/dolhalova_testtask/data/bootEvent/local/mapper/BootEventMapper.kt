package com.dolhalova_testtask.data.bootEvent.local.mapper

import com.dolhalova_testtask.data.bootEvent.local.entity.BootEventEntity
import com.dolhalova_testtask.domain.model.BootEvent
import java.util.Date

fun BootEventEntity.toModel() = BootEvent(
    id = this.id,
    occurredAt = Date(this.timestamp)
)