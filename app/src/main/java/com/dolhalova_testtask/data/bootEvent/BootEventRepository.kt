package com.dolhalova_testtask.data.bootEvent

import com.dolhalova_testtask.data.bootEvent.local.BootEventLocalSource
import com.dolhalova_testtask.domain.model.CreateBootEventRequestData
import com.dolhalova_testtask.domain.model.BootEvent
import kotlinx.coroutines.flow.Flow

class BootEventRepository(
    private val bootEventLocalSource: BootEventLocalSource
) {

    suspend fun createBootEvent(data: CreateBootEventRequestData) {
        bootEventLocalSource.createEvent(data)
    }

    suspend fun getLastEvents(limit: Int): List<BootEvent> {
        return bootEventLocalSource.getLastEvents(limit = limit)
    }

    fun getEvents(): Flow<List<BootEvent>> {
        return bootEventLocalSource.getEvents()
    }
}