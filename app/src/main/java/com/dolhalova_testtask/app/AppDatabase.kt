package com.dolhalova_testtask.app

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dolhalova_testtask.data.bootEvent.local.dao.BootEventDao
import com.dolhalova_testtask.data.bootEvent.local.entity.BootEventEntity

@Database(
    entities = [
        BootEventEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun bootEventDao(): BootEventDao
}