package com.dolhalova_testtask.app

import android.app.Application
import com.dolhalova_testtask.app.di.appModule
import com.dolhalova_testtask.app.di.dataModule
import com.dolhalova_testtask.app.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import timber.log.Timber.DebugTree
import com.dolhalova_testtask.BuildConfig
import com.dolhalova_testtask.app.di.viewModelModule

class BootCounterApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
        startKoin {
            if (BuildConfig.DEBUG) androidLogger(Level.ERROR)
            androidContext(this@BootCounterApp)
            modules(
                listOf(
                    appModule,
                    dataModule,
                    domainModule,
                    viewModelModule
                )
            )
        }
    }
}