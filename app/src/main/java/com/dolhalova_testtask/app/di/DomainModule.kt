package com.dolhalova_testtask.app.di

import com.dolhalova_testtask.domain.BootEventService
import org.koin.dsl.module

val domainModule = module {
    single {
        BootEventService(
            eventRepository = get(),
            dismissalSettingsRepository = get()
        )
    }
}