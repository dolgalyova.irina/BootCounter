package com.dolhalova_testtask.app.di

import android.content.Context
import androidx.room.Room
import com.dolhalova_testtask.app.AppDatabase
import com.dolhalova_testtask.common.arch.AppDispatchers
import com.dolhalova_testtask.common.notification.NotificationSender
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val APP_DATABASE_NAME = "app.db"

val appModule = module {
    single { createRoom(androidContext()) }
    single {
        AppDispatchers(
            background = Dispatchers.IO,
            observe = Dispatchers.Main
        )
    }
    single { NotificationSender(context = androidContext()) }
}


private fun createRoom(appContext: Context) = Room
    .databaseBuilder(appContext, AppDatabase::class.java, APP_DATABASE_NAME)
    .fallbackToDestructiveMigration()
    .build()