package com.dolhalova_testtask.app.di

import com.dolhalova_testtask.ui.BootEventReducer
import com.dolhalova_testtask.ui.BootEventStateModelMapper
import com.dolhalova_testtask.ui.BootEventViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { BootEventViewModel(
        bootEventService = get(),
        reducer = BootEventReducer(),
        stateToModelMapper = BootEventStateModelMapper(),
        dispatchers = get()
    ) }
}