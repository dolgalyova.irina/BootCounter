package com.dolhalova_testtask.app.di

import android.content.Context
import com.dolhalova_testtask.app.AppDatabase
import com.dolhalova_testtask.data.bootEvent.BootEventRepository
import com.dolhalova_testtask.data.bootEvent.local.BootEventLocalSource
import com.dolhalova_testtask.data.dismissalSettings.DismissalSettingsRepository
import com.dolhalova_testtask.data.dismissalSettings.local.DismissalSettingsLocalSource
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single { get<AppDatabase>().bootEventDao() }
    single { BootEventLocalSource(dao = get()) }
    single { BootEventRepository(bootEventLocalSource = get()) }
    single {
        val prefs = androidContext().getSharedPreferences(
            "notification_dismissal_settings.prefs",
            Context.MODE_PRIVATE
        )
        DismissalSettingsLocalSource(prefs = prefs)
    }
    single { DismissalSettingsRepository(localSource = get()) }
}