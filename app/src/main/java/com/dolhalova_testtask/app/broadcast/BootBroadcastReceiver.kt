package com.dolhalova_testtask.app.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.dolhalova_testtask.app.worker.CreateBootEventWorker
import com.dolhalova_testtask.app.worker.NotificationWorker
import java.util.concurrent.TimeUnit

class BootBroadcastReceiver : BroadcastReceiver() {

    companion object {
        const val ACTION_DEBUG_BOOT_EVENT = "com.dolhalova_testtask.ACTION_DEBUG_BOOT_EVENT"
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED || intent.action == ACTION_DEBUG_BOOT_EVENT) {
            scheduleWorkers(context)
            schedulePeriodicNotifications(context)
        }
    }

    private fun scheduleWorkers(context: Context) {
        val bootEventWorkRequest = OneTimeWorkRequestBuilder<CreateBootEventWorker>().build()
        val notificationWorkRequest = OneTimeWorkRequestBuilder<NotificationWorker>().build()

        WorkManager.getInstance(context)
            .beginWith(bootEventWorkRequest)
            .then(notificationWorkRequest)
            .enqueue()
    }

    private fun schedulePeriodicNotifications(context: Context) {
        val periodicWorkRequest = PeriodicWorkRequestBuilder<NotificationWorker>(
            15, TimeUnit.MINUTES
        ).build()

        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
            "PeriodicNotificationWork",
            ExistingPeriodicWorkPolicy.REPLACE,
            periodicWorkRequest
        )
    }
}