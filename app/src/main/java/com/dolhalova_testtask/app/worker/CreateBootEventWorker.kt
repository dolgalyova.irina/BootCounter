package com.dolhalova_testtask.app.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.dolhalova_testtask.common.arch.AppDispatchers
import com.dolhalova_testtask.domain.BootEventService
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

class CreateBootEventWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams), KoinComponent {
    private val bootEventService: BootEventService by inject()
    private val dispatchers: AppDispatchers by inject()

    override suspend fun doWork(): Result = withContext(dispatchers.background) {
        runCatching { bootEventService.createEvent() }.fold(
            onSuccess = { Result.success() },
            onFailure = {
                Timber.e(it)
                Result.failure()
            }
        )
    }
}