package com.dolhalova_testtask.app.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.dolhalova_testtask.R
import com.dolhalova_testtask.common.arch.AppDispatchers
import com.dolhalova_testtask.common.notification.NotificationFactory
import com.dolhalova_testtask.common.notification.NotificationSender
import com.dolhalova_testtask.domain.model.LastBootInfo
import com.dolhalova_testtask.domain.BootEventService
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NotificationWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams), KoinComponent {
    private val notificationSender: NotificationSender by inject()
    private val bootEventService: BootEventService by inject()
    private val dispatchers: AppDispatchers by inject()

    override suspend fun doWork(): Result = withContext(dispatchers.background) {
        try {
            val notificationText = when (val lastBootInfo = bootEventService.getLastBootInfo()) {
                is LastBootInfo.Absent -> applicationContext.getString(
                    R.string.notification_text_no_boots
                )

                is LastBootInfo.One -> applicationContext.getString(
                    R.string.notification_text_one_boot,
                    lastBootInfo.occurredAt
                )

                is LastBootInfo.Many -> applicationContext.getString(
                    R.string.notification_text_many_boots,
                    lastBootInfo.lastDelta.toString()
                )
            }

            val notification = NotificationFactory.create(
                context = applicationContext,
                notificationTitle = applicationContext.getString(R.string.notification_title),
                notificationText = notificationText
            )
            notificationSender.send(notification)

            val dismissalSettings = bootEventService.getDismissalSettings()
            val dismissals = dismissalSettings.dismissalCount
            if (dismissals < 5) {
                val delay = dismissals * 20L
                val delayedWorkRequest = OneTimeWorkRequestBuilder<NotificationWorker>()
                    .setInitialDelay(delay, TimeUnit.MINUTES)
                    .build()
                WorkManager.getInstance(applicationContext).enqueue(delayedWorkRequest)
            } else {
                schedulePeriodicNotifications(applicationContext)
            }

            Result.success()
        } catch (e: Exception) {
            Timber.e(e)
            Result.failure()
        }
    }

    private fun schedulePeriodicNotifications(context: Context) {
        val periodicWorkRequest = PeriodicWorkRequestBuilder<NotificationWorker>(
            15, TimeUnit.MINUTES
        ).build()

        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
            "PeriodicNotificationWork",
            ExistingPeriodicWorkPolicy.UPDATE,
            periodicWorkRequest
        )
    }
}
