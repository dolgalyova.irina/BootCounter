package com.dolhalova_testtask.ui

import com.dolhalova_testtask.common.arch.UIState
import com.dolhalova_testtask.domain.model.BootEvent
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings

data class BootEventState(
    val bootEvents: List<BootEvent>,
    val dismissalSettings: NotificationDismissalSettings?
) : UIState {
    companion object {
        val EMPTY = BootEventState(
            bootEvents = emptyList(),
            dismissalSettings = null
        )
    }
}