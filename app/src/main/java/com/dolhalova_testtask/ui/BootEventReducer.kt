package com.dolhalova_testtask.ui

import com.dolhalova_testtask.common.arch.Reducer

class BootEventReducer : Reducer<BootEventState, BootEventChange> {
    override fun reduce(state: BootEventState, change: BootEventChange): BootEventState {
        return when (change) {
            is BootEventChange.BootEventsLoaded -> state.copy(bootEvents = change.events)
            is BootEventChange.DismissalSettingsLoaded -> state.copy(dismissalSettings = change.settings)
        }
    }
}