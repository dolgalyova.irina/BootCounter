package com.dolhalova_testtask.ui

import com.dolhalova_testtask.common.arch.StateToModelMapper

class BootEventStateModelMapper : StateToModelMapper<BootEventState, BootEventModel> {
    override fun mapStateToModel(state: BootEventState): BootEventModel {
        return BootEventModel(
            bootEvents = state.bootEvents,
            dismissalCount = state.dismissalSettings?.dismissalCount?.toString().orEmpty(),
            dismissalInterval = state.dismissalSettings?.dismissalInterval?.toString().orEmpty()
        )
    }
}