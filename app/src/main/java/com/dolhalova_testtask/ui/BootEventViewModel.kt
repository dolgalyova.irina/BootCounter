package com.dolhalova_testtask.ui

import androidx.lifecycle.viewModelScope
import com.dolhalova_testtask.common.arch.AppDispatchers
import com.dolhalova_testtask.common.arch.FluxViewModel
import com.dolhalova_testtask.domain.BootEventService
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber

//TODO: implement error handling
class BootEventViewModel(
    private val bootEventService: BootEventService,
    reducer: BootEventReducer,
    stateToModelMapper: BootEventStateModelMapper,
    dispatchers: AppDispatchers,
) : FluxViewModel<BootEventAction, BootEventChange, BootEventState, BootEventModel>(
    initialState = BootEventState.EMPTY,
    dispatchers = dispatchers,
    reducer = reducer,
    stateToModelMapper = stateToModelMapper
) {

    init {
        loadEvents()
        loadSettings()
    }

    override fun processAction(action: BootEventAction) {
        when (action) {
            is BootEventAction.SaveDismissalSettings -> saveDismissalSettings(
                dismissals = action.dismissals,
                interval = action.interval
            )
        }
    }

    private fun loadEvents() {
        bootEventService.getEvents()
            .onEach { sendChange(BootEventChange.BootEventsLoaded(it)) }
            .catch { Timber.e(it) }
            .launchIn(viewModelScope)
    }

    private fun loadSettings() = viewModelScope.launch(dispatchers.background) {
        runCatching {
            val settings = bootEventService.getDismissalSettings()
            sendChange(BootEventChange.DismissalSettingsLoaded(settings))
        }
    }

    private fun saveDismissalSettings(dismissals: String, interval: String) {
        viewModelScope.launch(dispatchers.background) {
            //TODO: show validation error
            val dismissalsCount = dismissals.toIntOrNull() ?: return@launch
            val intervalTime = interval.toIntOrNull() ?: return@launch
            runCatching {
                val settings = NotificationDismissalSettings(
                    dismissalInterval = dismissalsCount,
                    dismissalCount = intervalTime
                )
                bootEventService.saveDismissalSettings(settings)
                sendChange(BootEventChange.DismissalSettingsLoaded(settings))
            }
        }
    }
}