package com.dolhalova_testtask.ui

import com.dolhalova_testtask.common.arch.UIAction
import com.dolhalova_testtask.common.arch.UIStateChange
import com.dolhalova_testtask.domain.model.BootEvent
import com.dolhalova_testtask.domain.model.NotificationDismissalSettings

sealed class BootEventAction : UIAction {
    data class SaveDismissalSettings(
        val dismissals: String,
        val interval: String
    ) : BootEventAction()
}

sealed class BootEventChange : UIStateChange {
    data class BootEventsLoaded(val events: List<BootEvent>) : BootEventChange()
    data class DismissalSettingsLoaded(
        val settings: NotificationDismissalSettings
    ) : BootEventChange()
}