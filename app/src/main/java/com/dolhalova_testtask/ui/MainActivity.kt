package com.dolhalova_testtask.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.dolhalova_testtask.R
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {
    private val bootEventsInfo by lazy {
        findViewById<TextView>(R.id.bootEventsInfo)
    }
    private val dismissalCountInput by lazy {
        findViewById<TextInputEditText>(R.id.dismissalCountInput)
    }
    private val dismissalIntervalInput by lazy { findViewById<TextInputEditText>(R.id.dismissalIntervalInput) }
    private val saveButton by lazy { findViewById<Button>(R.id.saveButton) }
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (!isGranted) {
            //TODO: show warning
        }
    }
    private val viewModel: BootEventViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        observeModel()
        checkNotificationPermission()
    }

    private fun observeModel() {
        viewModel.model
            .filterNotNull()
            .onEach { model ->
                bootEventsInfo.text = model.bootEvents.joinToString("\n") { event ->
                    "Boot time: ${event.occurredAt}"
                }
            }
            .launchIn(lifecycleScope)
    }

    private fun initViews() {
        bootEventsInfo.movementMethod = ScrollingMovementMethod()
        saveButton.setOnClickListener {
            viewModel.dispatch(
                BootEventAction.SaveDismissalSettings(
                    dismissals = dismissalCountInput.text.toString(),
                    interval = dismissalIntervalInput.text.toString()
                )
            )
        }
    }

    private fun checkNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            when (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS)) {
                PackageManager.PERMISSION_GRANTED -> Unit
                else -> {
                    requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
                }
            }
        }
    }
}
