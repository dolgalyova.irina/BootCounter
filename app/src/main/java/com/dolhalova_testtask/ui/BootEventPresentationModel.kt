package com.dolhalova_testtask.ui

import com.dolhalova_testtask.common.arch.UIModel
import com.dolhalova_testtask.domain.model.BootEvent

data class BootEventModel(
    val bootEvents: List<BootEvent>,
    val dismissalInterval: String,
    val dismissalCount: String
) : UIModel