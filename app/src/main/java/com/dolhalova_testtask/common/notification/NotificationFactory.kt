package com.dolhalova_testtask.common.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.content.getSystemService

object NotificationFactory {
    private const val CHANNEL_ID = "boot_event_channel"
    private const val CHANNEL_NAME = "Boot Events"

    fun create(
        context: Context,
        notificationTitle: String,
        notificationText: String,
        @DrawableRes iconId: Int = android.R.drawable.ic_menu_compass,
        channelId: String = CHANNEL_ID,
        channelName: String = CHANNEL_NAME,
        channelImportance: Int = NotificationManager.IMPORTANCE_DEFAULT
    ): Notification {
        val notificationManager = context.getSystemService<NotificationManager>()
            ?: error("Can't get a NotificationManager from received context")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                channelName,
                channelImportance
            )
            notificationManager.createNotificationChannel(channel)
        }

        return NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle(notificationTitle)
            .setContentText(notificationText)
            .setSmallIcon(iconId)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .build()
    }
}
