package com.dolhalova_testtask.common.arch

typealias ErrorHandler = (Throwable) -> Unit