package com.dolhalova_testtask.common.arch

import kotlinx.coroutines.CoroutineDispatcher

data class AppDispatchers(
    val background: CoroutineDispatcher,
    val observe: CoroutineDispatcher
)